#include <iostream>

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_native_dialog.h>

using namespace std;

class Player
{
	ALLEGRO_BITMAP  *image;

public:
	Player() {
		image = al_load_bitmap("imagen.jpg");
	}

	void render(){
		al_draw_bitmap(image, 50, 50, 0);
	}

	void destroy() {
		al_destroy_bitmap(image);
	}
};

int main(int argc, char **argv){

   ALLEGRO_DISPLAY *display = NULL;

   if(!al_init()) {
      al_show_native_message_box(display, "Error", "Error", "Failed to initialize allegro!", 
                                 NULL, ALLEGRO_MESSAGEBOX_ERROR);
      return 0;
   }

   if(!al_init_image_addon()) {
      al_show_native_message_box(display, "Error", "Error", "Failed to initialize al_init_image_addon!", 
                                 NULL, ALLEGRO_MESSAGEBOX_ERROR);
      return 0;
   }

   display = al_create_display(800,600);

   if(!display) {
      al_show_native_message_box(display, "Error", "Error", "Failed to initialize display!", 
                                 NULL, ALLEGRO_MESSAGEBOX_ERROR);
      return 0;
   }

   Player player;


   player.render();
   
   
   al_flip_display();
   al_rest(2);
   cin.get();
   al_destroy_display(display);


   player.destroy();

   return 0;
}